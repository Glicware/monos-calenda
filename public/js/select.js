$(document).ready(function() {
    var id = $('.c').val();
    console.log(id);


    $.ajax({
        url: '/propietarioFind',
        dataType: 'json',
        delay: 250,
        error: function(){
            alert("Error en la petición a la base de datos");
        }
    }).done( function(valor) {
       var texto = "";
       //Recorremos los valores con un for each.
       $.each(function(i, valor){
           texto += "<option value='"+valor.id+"'>"+valor.nombre+"</option>";
       });
       //Agregamos en el id "idprovincia" el valor que tenga la variable texto. (todas las provincias).
       $('.propietario_id').html(texto);
    });
    $('.predio-propietario-id').select2.defaults.set('1', 'Oscar');



    $('.sel').focus(function(){
      var name = $(this).attr('class');
      var className = name.split(" ");
      console.log(className[1]);
      $('.'+ className[1]).select2({
        language: {
          errorLoading: function() {
            return "La carga falló"
          },
          inputTooLong: function(e) {
            var t = e.input.length - e.maximum,
              n = "Por favor, elimine " + t + " car";
            return t == 1 ? n += "ácter" : n += "acteres", n
          },
          inputTooShort: function(e) {
            var t = e.minimum - e.input.length,
              n = "Por favor, introduzca " + t + " car";
            return t == 1 ? n += "ácter" : n += "acteres", n
          },
          loadingMore: function() {
            return "Cargando más resultados…"
          },
          maximumSelected: function(e) {
            var t = "Sólo puede seleccionar " + e.maximum + " elemento";
            return e.maximum != 1 && (t += "s"), t
          },
          noResults: function() {
            return "No se encontraron resultados"
          },
          searching: function() {
            return "Buscando…"
          }
        },
        ajax: {
          url: '/propietarioFind',
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
              results:  $.map(data, function (item) {
                    return {
                        text: item.nombre + " " + "(" + item.curp + ")",
                        id: item.id
                    }
                })
            };
          },
          cache: true
        }
    });
  });
});
