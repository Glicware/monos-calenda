<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cotizacion extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cotizacions';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['id_paquete', 'id_cliente'];

    public function paquetes()
    {
        return $this->belongsTo('App\Paquete');
    }
    public function cliente()
    {
        return $this->belongsTo('App\Cliente');
    }
    
}
