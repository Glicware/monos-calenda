<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Evento;
use Illuminate\Http\Request;
use App\Paquete;
use App\Cliente;
use App\User;
class EventoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $evento = Evento::where('fecha', 'LIKE', "%$keyword%")
                ->orWhere('id_user', 'LIKE', "%$keyword%")
                ->orWhere('id_cliente', 'LIKE', "%$keyword%")
                ->orWhere('id_paquete', 'LIKE', "%$keyword%")
                ->orWhere('hora_inicio', 'LIKE', "%$keyword%")
                ->orWhere('hora_fin', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $evento = Evento::latest()->paginate($perPage);
        }

        return view('evento.index', compact('evento'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $paquetes = Paquete::pluck('nombre', 'id')->all();
        $clientes = Cliente::pluck('nombre', 'id')->all();
        $users = User::pluck('name', 'id')->all();
        return view('evento.create', compact('paquetes', 'clientes', 'users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'id_paquete' => 'required',
			'id_cliente' => 'required',
			'fecha' => 'required',
			'hora_inicio' => 'required',
			'hora_fin' => 'required'
		]);
        $requestData = $request->all();

        Evento::create($requestData);

        return redirect('evento')->with('flash_message', 'Evento added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $evento = Evento::findOrFail($id);

        return view('evento.show', compact('evento'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $evento = Evento::findOrFail($id);

        return view('evento.edit', compact('evento'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'id_paquete' => 'required',
			'id_cliente' => 'required',
			'fecha' => 'required',
			'hora_inicio' => 'required',
			'hora_fin' => 'required'
		]);
        $requestData = $request->all();

        $evento = Evento::findOrFail($id);
        $evento->update($requestData);

        return redirect('evento')->with('flash_message', 'Evento updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Evento::destroy($id);

        return redirect('evento')->with('flash_message', 'Evento deleted!');
    }
}
