<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Cotizacion;
use Illuminate\Http\Request;
use App\Cliente;
use App\Paquete;

class CotizacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $cotizacion = Cotizacion::where('id_paquete', 'LIKE', "%$keyword%")
                ->orWhere('id_cliente', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $cotizacion = Cotizacion::latest()->paginate($perPage);
        }

        return view('cotizacion.index', compact('cotizacion'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $paquetes = Paquete::pluck('nombre', 'id')->all();
        $clientes = Cliente::pluck('nombre', 'id')->all();
        return view('cotizacion.create', compact('paquetes', 'clientes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'id_paquete' => 'required',
			'id_cliente' => 'required'
		]);
        $requestData = $request->all();

        Cotizacion::create($requestData);

        return redirect('cotizacion')->with('flash_message', 'Cotizacion added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $cotizacion = Cotizacion::findOrFail($id);

        return view('cotizacion.show', compact('cotizacion'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $cotizacion = Cotizacion::findOrFail($id);

        return view('cotizacion.edit', compact('cotizacion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'id_paquete' => 'required',
			'id_cliente' => 'required'
		]);
        $requestData = $request->all();

        $cotizacion = Cotizacion::findOrFail($id);
        $cotizacion->update($requestData);

        return redirect('cotizacion')->with('flash_message', 'Cotizacion updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Cotizacion::destroy($id);

        return redirect('cotizacion')->with('flash_message', 'Cotizacion deleted!');
    }
}
