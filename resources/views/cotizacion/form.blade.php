<div class="form-group {{ $errors->has('id_paquete') ? 'has-error' : ''}}">
    <label for="id_paquete" class="col-md-4 control-label">{{ 'Id Paquete' }}</label>
    <div class="col-md-6">
      {!! Form::select('id_paquete', $paquetes, null, ['class' => 'form-control', 'required', 'placeholder' => 'Seleccione un paquetes']); !!}

        {!! $errors->first('id_paquete', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('id_cliente') ? 'has-error' : ''}}">
    <label for="id_cliente" class="col-md-4 control-label">{{ 'Id Cliente' }}</label>
    <div class="col-md-6">
      {!! Form::select('id_cliente', $clientes, null, ['class' => 'form-control','placeholder' => 'Seleccione un cliente']); !!}
        {!! $errors->first('id_cliente', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        <input class="btn btn-primary" type="submit" value="{{ $submitButtonText or 'Create' }}">
    </div>
</div>
