<div class="form-group {{ $errors->has('nombre') ? 'has-error' : ''}}">
    <label for="nombre" class="col-md-4 control-label">{{ 'Nombre' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="nombre" type="text" id="nombre" value="{{ $evento->nombre or ''}}" required>
        {!! $errors->first('nombre', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('fecha') ? 'has-error' : ''}}">
    <label for="fecha" class="col-md-4 control-label">{{ 'Fecha' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="fecha" type="date" id="fecha" value="{{ $evento->fecha or ''}}" required>
        {!! $errors->first('fecha', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('id_user') ? 'has-error' : ''}}">
    <label for="id_user" class="col-md-4 control-label">{{ 'Id User' }}</label>
    <div class="col-md-6">
      {!! Form::select('id_user', $users, null, ['class' => 'form-control','placeholder' => 'Seleccione un usuario']); !!}
        {!! $errors->first('id_user', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('id_cliente') ? 'has-error' : ''}}">
    <label for="id_cliente" class="col-md-4 control-label">{{ 'Id Cliente' }}</label>
    <div class="col-md-6">
      {!! Form::select('id_cliente', $clientes, null, ['class' => 'form-control','placeholder' => 'Seleccione un cliente']); !!}
        {!! $errors->first('id_cliente', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('id_paquete') ? 'has-error' : ''}}">
    <label for="id_paquete" class="col-md-4 control-label">{{ 'Id Paquete' }}</label>
    <div class="col-md-6">
      {!! Form::select('id_paquete', $paquetes, null, ['class' => 'form-control','placeholder' => 'Seleccione un paquete']); !!}
        {!! $errors->first('id_paquete', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('hora_inicio') ? 'has-error' : ''}}">
    <label for="hora_inicio" class="col-md-4 control-label">{{ 'Hora Inicio' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="hora_inicio" type="datetime-local" id="hora_inicio" value="{{ $evento->hora_inicio or ''}}" required>
        {!! $errors->first('hora_inicio', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('hora_fin') ? 'has-error' : ''}}">
    <label for="hora_fin" class="col-md-4 control-label">{{ 'Hora Fin' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="hora_fin" type="datetime-local" id="hora_fin" value="{{ $evento->hora_fin or ''}}" required>
        {!! $errors->first('hora_fin', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        <input class="btn btn-primary" type="submit" value="{{ $submitButtonText or 'Create' }}">
    </div>
</div>
