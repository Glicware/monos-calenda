<div class="form-group {{ $errors->has('id_evento') ? 'has-error' : ''}}">
    <label for="id_evento" class="col-md-4 control-label">{{ 'Id Evento' }}</label>
    <div class="col-md-6">
      {!! Form::select('id_evento', $eventos, null, ['class' => 'form-control','placeholder' => 'Seleccione un evento']); !!}
        {!! $errors->first('id_evento', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('id_user') ? 'has-error' : ''}}">
    <label for="id_user" class="col-md-4 control-label">{{ 'Id User' }}</label>
    <div class="col-md-6">
      {!! Form::select('id_user', $users, null, ['class' => 'form-control','placeholder' => 'Seleccione un usuario']); !!}
        {!! $errors->first('id_user', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('total') ? 'has-error' : ''}}">
    <label for="total" class="col-md-4 control-label">{{ 'Total' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="total" type="number" id="total" value="{{ $pago->total or ''}}" required>
        {!! $errors->first('total', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('fecha') ? 'has-error' : ''}}">
    <label for="fecha" class="col-md-4 control-label">{{ 'Fecha' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="fecha" type="date" id="fecha" value="{{ $pago->fecha or ''}}" required>
        {!! $errors->first('fecha', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        <input class="btn btn-primary" type="submit" value="{{ $submitButtonText or 'Create' }}">
    </div>
</div>
