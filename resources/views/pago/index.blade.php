@extends('adminlte::layouts.app')
@section('main-content')
  <div class="container">
      <div class="row">
          <div class="col-md-10">
              <div class="panel panel-default">
                  <div class="panel-heading">Pagos</div>
                  <div class="panel-body">
                      <a href="{{ url('/pago/create') }}" class="btn btn-success btn-sm" title="Registrar nuevo concepto">
                          <i class="fa fa-plus" aria-hidden="true"></i> Registrar Nuevo
                      </a>

                      {!! Form::open(['method' => 'GET', 'url' => '/concepto', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                      <div class="input-group">
                          <input type="text" class="form-control" name="search" placeholder="Buscar...">
                          <span class="input-group-btn">
                              <button class="btn btn-default" type="submit">
                                  <i class="fa fa-search"></i>
                              </button>
                          </span>
                      </div>
                      {!! Form::close() !!}

                      <br/>
                      <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Id Evento</th><th>Id User</th><th>Total</th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($pago as $item)
                                    <tr>
                                        <td>{{ $loop->iteration or $item->id }}</td>
                                        <td>{{ $item->id_evento }}</td><td>{{ $item->id_user }}</td><td>{{ $item->total }}</td>
                                        <td>
                                            <a href="{{ url('/pago/' . $item->id) }}" title="View Pago"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                            <a href="{{ url('/pago/' . $item->id . '/edit') }}" title="Edit Pago"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                                            <form method="POST" action="{{ url('/pago' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger btn-sm" title="Delete Pago" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $pago->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
